defmodule KdsListener do
  @moduledoc """
  An example GenServer showing one way of using :gen_tcp to open a socket and
  listen for messages on a specific port.
  """

  use GenServer
  require Logger

  # This is a type definition. It's not used by the compiler, but can be used
  # by code analysis tools like `dialyzer` to make sure our functions are doing
  # what we say they're doing.
  # I mostly just use them like documentation to keep track of what types I'm
  # expecting in a function.
  @type state :: %{
    port: integer(),
    socket: :gen_tcp.socket(),
    received_messages: [any()]
  }

  # Client API
  @doc """
  Start a new process which opens a socket and listens on a specific port.

  ## Examples
    iex> KdsListener.start_link(9100)
    {:ok, new_pid}
  """
  def start_link(port) do
    # This call turns around an internall calls our `init` function defined
    # below. The difference is that this `start_link` function is called in the
    # context of the caller's process, whereas `init` is called from within the
    # newly-created process.
    GenServer.start_link(__MODULE__, port)
  end

  @doc """
  Stop a KdsListener process

  ## Examples
    iex> {:ok, pid} = KdsListener.start_link(9100)
    iex> KdsListener.stop(pid)
    :ok
  """
  def stop(pid) do
    GenServer.stop(pid)
  end

  # Just a handy function to see what messages our kds listener has received
  def get_messages(pid) do
    GenServer.call(pid, :get_messages)
  end

  # GenServer Callbacks
  # init creates a socket listening on `port` and holds onto the socket's pid
  # in its own internal state. This way we can shut down the socket when we
  # want to terminate the KdsListener process
  def init(port) do
    IO.puts "in init for port #{port}...."
    # There are several options we can pass to the `listen` function as the
    # second parameter, depending on if we want to actively or passively listen,
    # etc. We'll actively listen, meaning our process will receive a message
    # each time a new packet is received. This will trigger our `handle_info`
    # function below.
    case :gen_tcp.listen(port, [:binary]) do
      {:ok, socket} ->
        IO.puts "got socket..."

        # Got a socket, start accepting connections to it in an async process
        my_pid = self()
        _acceptor_pid = spawn_link(fn ->
          accept_connection(socket, port, my_pid)
        end)

        new_state = %{
          socket: socket,
          port: port,
          received_messages: []
        }

        # Returning `{:ok, state}` from `init` tells GenServer that our process
        # startup was successful and what our processes state is.
        {:ok, new_state}
      {:error, reason} ->
        Logger.error "(KdsListener) Couldn't listen on port #{port}: #{inspect reason}"
        {:stop, reason, %{}}
    end
  end

  # handle_call is a GenServer callback that is called automatically when we
  # receive a GenServer.call. Calls are synchronous, so our process will block
  # while this function executes.
  def handle_call(:get_messages, _from, state) do
    {:reply, state[:received_messages], state}
  end

  def handle_info({:new_client_socket, client_socket}, state) do
    Logger.debug "(KdsListener) New client socket connected on port #{state[:port]}: #{inspect client_socket}."
    # TODO: Maybe keep track of the clients...

    {:noreply, state}
  end

  # handle_info is a GenServer callback that is called automatically when we
  # receive a message in our processes mailbox from some external process
  def handle_info({:tcp, _from, message}, state) do
    Logger.debug "Socket on port #{state[:port]} just got message: #{inspect message}"

    # Append our new message to our list of messages
    new_received_messages = state[:received_messages] ++ [message]

    # Update our state with the modified list
    new_state = Map.put(state, :received_messages, new_received_messages)

    # Return our new state
    {:noreply, new_state}
  end

  def handle_info({:tcp_closed, _from}, state) do
    # Just means a particular client disconnected
    Logger.debug "(KdsListener) Client on port #{state[:port]} disconnected."
    {:noreply, state}
  end

  # terminate is a GenServer callback that is called automatically (usually)
  # when our process should shut down
  def terminate(reason, state) do
    Logger.info "Socket on port #{state[:port]} shutting down for reason: #{inspect reason}."

    if state[:socket] != nil do
      :gen_tcp.close(state[:socket])
    end

    state
  end

  # Helper functions
  defp accept_connection(socket, port, parent_pid) do
    # Block with no timeout, since this is running in its own process.
    case :gen_tcp.accept(socket) do
      {:ok, client_socket} ->
        # Set the parent process as the owner of this client socket. This is how
        # the parent process will still be the process to receive the messages
        # from the client.
        :ok = :gen_tcp.controlling_process(client_socket, parent_pid)
        send parent_pid, {:new_client_socket, client_socket}

        # Recursively call into this function so we continue to accept new
        # client connections.
        accept_connection(socket, port, parent_pid)
      {:error, :closed} ->
        Logger.info "(KdsListener) Socket acceptor on port #{port} closed."
        :ok
      {:error, other} ->
        # Some other error occurred waiting on a connection. No idea how to
        # handle this, so let's just crash our current process.
        Logger.error "(KdsListener) Socket acceptor on port #{port} got an unexpected error: #{inspect other}. Shutting down."
        :error
    end
  end
end

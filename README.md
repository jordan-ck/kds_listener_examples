# KdsListener

**An example gen_tcp application**

Run it with `iex -S mix`


```Elixir
# Spin up some kds listener processes like so:
{:ok, listener_9100_pid} = KdsListener.start_link(9100)

# Open up some sockets as clients
{:ok, socket_9100}  = :gen_tcp.connect({127, 0, 0, 1}, 9100, [])
{:ok, socket_9100a} = :gen_tcp.connect({127, 0, 0, 1}, 9100, [])

# Send some messages
:gen_tcp.send(socket_9100, "Sup 9100?")
:gen_tcp.send(socket_9100a, "Hey I am a different client on port 9100!")

# Shut down a client after its finished sending messages
:gen_tcp.close(socket_9100)
:gen_tcp.close(socket_9100a)

# Check what messages our kds listeners have received
KdsListener.get_messages(listener_9100_pid)

# To cleanly shut down the socket listener:
GenServer.stop(listener_9100_pid)

```